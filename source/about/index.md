---
title: Créditos
date: 2018-05-02
comment: false
---

Mi nombre es [Romeo LopCam](../), nací en México y este es mi blog personal.

Contar historias relevantes, investigar a profundidad, ser claro y preciso, no aburrirme, ni aburrir; son algunos de los objetivos que me guían cada que me siento a escribir.

En este lugar voy recopilando aquellas notas, artículos, ensayos, crónicas y entrevistas que puedo releer sin sentir remordimientos.

Cualquier asunto relacionado con los contenidos de este espacio por favor escribe al correo: [romeolopcam@gmail.com](mailto:romeolopcam@gmail.com)

## Herramientas

Este blog está hospedado en [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) y se construye con ayuda de [Hexo](https://hexo.io), un potente [generador de sitios estáticos](https://www.staticgen.com/) de código abierto.

## Licencia

Salvo indicación contraria todos los trabajos están publicados bajo una licencia [Creative Commons BY-NC-ND 4.0](http://creativecommons.org/licenses/by-nc-nd/4.0/deed.es). Esto quiere decir que pueden distribuirse sin ánimo de lucro, siempre y cuando se respete la integridad de cada material y se cite la fuente.
