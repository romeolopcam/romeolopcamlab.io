---
title: 'Mi experiencia con el Viagra'
author: Romeo LopCam
date: 2014-11-26
categories: Crónica
tags: [viagra, medicina, laboratorios]
---

![Rata de laboratorio.](/img/rata01.jpg)

Durante la primavera pasada me convertí en uno de los poco más de 25 mil 500 conejillos de indias en los que experimenta la industria de los medicamentos genéricos del país. Por prestarme a ello recibí una compensación de 3 mil 500 pesos libre de impuestos, luego de estar internado durante 42 horas aproximadamente, en una de las 21 Unidades Clínicas de Terceros Autorizados, avaladas por la Comisión Federal para Prevenir Riesgos Sanitarios (Cofepris). Les vendí, literalmente, 90 mililitros de mi sangre.

<!-- more -->

Dichas unidades —gestionadas por empresas particulares que son contratadas por los laboratorios interesados— se enfocan en realizar los denominados «estudios de bioequivalencia», que sirven para emitir una «opinión técnica» ante la autoridad correspondiente, en donde se comparan los efectos de un medicamento genérico que aún no ha salido al mercado, con los de su correspondiente medicamento de referencia, es decir, el de patente.

Mi contacto, un químico farmacéutico que participó en estos ensayos en su época de estudiante, me remitió —según esto— a una de las mejor equipadas y en donde «mejor te atienden», el Centro de Estudios Científicos y Clínicos Pharma. Tal valoración fue confirmada por algunos de mis compañeros de internamiento con experiencia en la materia, aunque la verdad a mí el ambiente me pareció bastante deprimente. Quizá era ese color azul pálido de los sillones de vinipiel, o la luz blanca de las lámparas fluorescentes. No sé. El caso es que en cuanto me dieron el uniforme hospitalario, supe que ese no era mi lugar. Sin embargo continué, diciéndome a mí mismo que las horas pasarían rápido.

«Deberías de ver el que está por Copilco, o Médica Sur» me dijo en cierto momento Miguel, el único amigo que pude hacer durante esta investigación, «en el primero hay hasta chinches y en el otro son bien marros con la comida». No he podido comprobar sus aseveraciones pero le creí. Miguel tiene 23 años, está casado, tienen dos hijos pequeños y hasta hace no mucho vivía de esto. «Ahora te meten a la base de datos de la Cofepris y no puedes entrar a otro estudio sino hasta después de dos meses», me dijo dibujando una mueca en su rostro moreno, en el que resaltaban un par de diminutos ojos pardos.

Pero regresemos un poco en el tiempo. Para entrar a un estudio de bioequivalencia es requisito estar sano, por lo que en una primera etapa uno tiene que pasar por proceso de selección que incluye un chequeo médico general y estudios de laboratorio básicos, tales como biometría hemática completa, química sanguínea de 27 elementos y un examen de orina. Aunque como pude observar el día que acudí a ofrecerme como paciente voluntario, el primer «filtro» que aplican es el de no estar obeso, varias personas fueron rechazadas mientras me encontraba en la sala de espera por este motivo. Por otro lado no puedo dejar de apuntar que apenas una semana antes me había comido un pequeño panqué con marihuana, bastante potente, por lo que temía ser rechazado en éste primer intento. Sin embargo eso no sucedió.

Una vez aceptado guardan tus datos en una base, en espera de que se abra un estudio. Así, me llamaron dos semanas después de las pruebas iniciales para ofrecerme dos opciones. La primera era probar un medicamento para regular el colesterol, por lo que pagaban 7 mil pesos y tenías que internarte durante cuatro ocasiones. Mientras que la segunda era probar una pastilla de  50 mg de Sildenafil (sustancia activa del famoso Viagra), por lo que me pagarían 3 mil 500 pesos y estaría internado sólo en una ocasión. Como resulta obvio, la segunda propuesta me pareció más interesante.

Ingresé a la clínica un viernes por la tarde y salí el domingo por la mañana, pero fue sólo durante todo el día sábado que los «vampiros» de la farmacología genérica estuvieron succionando periódicamente muestras de mi sangre y almacenándolas en tubos de 4.5 mililitros. Para ello me colocaron un catéter en una de mis venas más propicias, con el objeto de no estarme pinchando una y otra vez. Tener un tubo de plástico insertado en mi brazo fue de las cosas más molestas que tuve que soportar, pero bueno, al final te acabas acostumbrando.

Toda esa mañana, de las 7 hasta las 11, fue frenética. Cinco enfermeras apenas y se daban abasto extrayendo la sangre de 40 pacientes voluntarios, a intervalos que fueron haciéndose cada vez más largos. Es decir, en la primera hora se realizaron seis tomas, en la segunda cuatro, en la tercera cuatro, en la cuarta dos, en la quinta una y a partir de ahí, una cada dos, cuatro y seis horas; dando un total de 20 muestras. Almorzamos hacia las 12, comimos a las 3 de la tarde y cenamos a las 10 de la noche. Durante toda la jornada seguimos estrictamente los horarios, lo que me hizo pensar en [Michel Foucault][1] y su análisis del [*poder disciplinario*][2], en el que afirma que clínicas, escuelas, cuarteles militares y prisiones, operan bajo un principio común.

Me llamó la atención también que cuando la enfermera que me fue asignada trastabilló con uno de los tubos llenos de sangre en su mano, luego de tropezarse con el pie de un paciente, el director del centro saltó hacia ella y se lo quitó mientras le decía: «¡Cuidado, que esto es oro puro!». Rumores que no pude comprobar afirman que por los 90 mililitros de sangre de cada voluntario, los laboratorios le pagan a los dueños de estas unidades de investigación entre 125 mil y 200 mil pesos. Quizá es una exageración. Aunque seguramente es mucho más que los 3 mil 500 que ellos te entregan.

En cuanto a los efectos del Sildenafil o Viagra, lo único que pude notar es que se me enrojeció ligeramente la cara y mis ojos se mostraron hipersensibles ante la luz hasta la hora de la comida. No tuve ninguna erección, ya que como advierten los propios laboratorios, el medicamento no es mágico y sólo facilita el adecuado flujo de sangre hacia el pene cuando hay estimulación. O sea que para bien o para mal, no hubo nada excitante para mí en el entorno. Eso sí, uno de mis compañeros de habitación cuyo nombre era Luis Armando pero al que muchos —ignoro porqué— le decían *El George*, tuvo a bien informarnos que nunca la había tenido tan dura, luego de que fue a «jalársela» al baño.

Semanas después una amiga me recordó que el maestro del periodismo de inmersión, [Günter Wallraff][3], se sometió a una experiencia similar mientras vivió disfrazado de migrante en Alemania, misma que narró en uno de los capítulos de su libro *Cabeza de turco*. En él se hace —a partir de una investigación más extensa— una denuncia demoledora del funcionamiento de los ensayos clínicos en dicho país europeo. Por lo que pude observar aquí, muchos de esos vicios se han replicado.

Varios de los participantes recordaban la época en que podían dedicarse a vender su sangre sin límites, «salías de uno y en corto te metías al otro» me comentó Miguel, «conozco a uno al que se le empezaron a caer mechones de pelo». Sin embargo cuando le pregunté porqué lo hacía si sabía de esos riesgos, me contestó algo que era de esperarse, «lo que te pagan por un trabajo de 8 horas diarias en cualquier empresa rascuache, lo saco aquí en dos internamientos, bueno, lo sacaba». Ahora es «promotor» y le pagan 200 pesos por cada paciente voluntario recomendado, «pero no es lo mismo».

Salí de la clínica hacia las 8 de la mañana del día domingo con un sandwich de ensalada de pollo envuelto en papel aluminio, un yoghurt, una manzana y un cheque de 3 mil 500 pesos en mis manos. Caminé con Miguel hasta el metro Etiopía y aproveché para animarlo a acabar la preparatoria en un sistema de educación a distancia sobre el que tenía algo de información. Me contestó muy seriamente que lo tomaría en cuenta antes de despedirse con un fuerte apretón de manos. Lo vi alejarse la mar de contento, observando los números escritos en su respectivo pedazo de papel que en breve cambiaría por dinero.

----

Publicado en *[Yaconic][4]* el 26 de noviembre de 2014.
Así como en el Número 14, Año 2, de su versión impresa.

[1]: http://es.wikipedia.org/wiki/Michel_Foucault
[2]: http://es.wikipedia.org/wiki/Poder_disciplinario
[3]: http://es.wikipedia.org/wiki/G%C3%BCnter_Wallraff
[4]: http://www.yaconic.com/mi-experiencia-con-el-viagra/
