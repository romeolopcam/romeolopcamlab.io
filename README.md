# Créditos

Mi nombre es [Romeo LopCam](https://romeolopcam.gitlab.io), nací en México y este es mi blog personal.

Contar historias relevantes, investigar a profundidad, ser claro y preciso, no aburrirme, ni aburrir; son algunos de los objetivos que me guían cada que me siento a escribir.

En este lugar voy recopilando aquellas notas, artículos, ensayos, crónicas y entrevistas que puedo releer sin sentir remordimientos.

Cualquier asunto relacionado con los contenidos de este espacio por favor escribe al correo: [romeolopcam@gmail.com](mailto:romeolopcam@gmail.com)

## Herramientas

Este blog está hospedado en [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) y se construye con ayuda de [Hexo](https://hexo.io), un potente [generador de sitios estáticos](https://www.staticgen.com/) de código abierto.

## Licencia

Salvo indicación contraria todos los trabajos están publicados bajo una licencia [Creative Commons BY-NC-ND 4.0](http://creativecommons.org/licenses/by-nc-nd/4.0/deed.es). Esto quiere decir que pueden distribuirse sin ánimo de lucro, siempre y cuando se respete la integridad de cada material y se cite la fuente.

## Instalación

1. Instala [nodejs](https://nodejs.org/) y [npm](https://www.npmjs.com/).
2. Instala [hexo](https://hexo.io/) con `sudo npm install -g hexo-cli`.
3. Asegúrate haber migrado tus claves SSH.
4. Asegúrate de haber migrado tu archivo `.gitconfig`.
5. Clona el repositorio en tu carpeta `GitLab` con SSH:

```zsh
git clone git@gitlab.com:romeolopcam/romeolopcam.gitlab.io.git
```

6. Entra en la carpeta e instala los módulos de node con `npm install`.
7. Haz algunos cambios con tu editor de código preferido.
8. Crea el siguiente alias en tu archivo .zshrc para actualizar el repositorio con el comando `gitsync`.

```zsh
alias gitsync="git add . && git commit -m '`date +'%d de %b %X'`'&& git push"
```

9. Verifica los cambios [en linea](https://romeolopcam.gitlab.io).

## Hexo en GitLab Pages

Consulta cómo desplegar [Hexo](https://hexo.io/) en [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) en [este enlace](https://hexo.io/docs/gitlab-pages.html) y ve una plantilla de ejemplo en [este otro](https://gitlab.com/pages/hexo).

